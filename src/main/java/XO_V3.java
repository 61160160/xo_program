
import java.util.Arrays;
import java.util.Scanner;

public class XO_V3 {

    public static String[] channel = new String[9];
    public static String player = "X";
    public static int numInput, turn = 0;
    public static String line;

    private static void showWelcome() {
        System.out.println("Hello welcome to XO Game ");
        System.out.println(" Let's Start");
    }

    private static void setChannel() {
        for (int i = 0; i < 9; i++) {
            channel[i] = String.valueOf(i + 1);
        }
    }

    private static void showTable() {
        System.out.println("-------------");
        System.out.println(" | " + channel[0] + "  " + channel[1]
                + "  " + channel[2] + " |");
        System.out.println(" | " + channel[3] + "  " + channel[4] + "  "
                + channel[5] + " |");
        System.out.println(" | " + channel[6] + "  " + channel[7] + "  "
                + channel[8] + " |");
        System.out.println("-------------");
    }

    private static void showTurn() {
        System.out.println(player + " turn");
    }

    private static void input() {
        Scanner kb = new Scanner(System.in);
        do {
            System.out.println("Please select slot:");
            numInput = kb.nextInt();
        } while (checkPosition(numInput) == false);
        if (channel[numInput - 1].equals(String.valueOf(numInput))) {
            channel[numInput - 1] = player;
        }
    }

    private static boolean checkPosition(int numInput) {
        if (!(numInput > 0 && numInput <= 9)) {
            System.out.println("Error: don't have position!!,Please try again.");
            return false;
        } else if (channel[numInput - 1] == "X" || channel[numInput - 1] == "O") {
            System.err.println("Error: Slot is not Empty!!,Please try agian.");
            return false;
        } else {
            return true;
        }
        
    }


    private static boolean checkwin() {
        if (checkRow()) {
            return true;
        }
        if (checkcolum()) {
            return true;
        }
        if (checkDiagonal()) {
            return true;
        }
        if (isDraw()) {
            return true;
        }
        return false;
    }

    private static boolean checkRow() {
        if (channel[0] == channel[1] && channel[2] != "3") {
            if (channel[0] == channel[2]) {
                return true;
            }}
        if (channel[3] == channel[4] && channel[5] != "6") {
            if (channel[3] == channel[5]) {
                return true;
            }}
        if (channel[6] == channel[7] && channel[8] != "9") {
            if (channel[6] == channel[8]) {
                return true;
            }}
        return false;
    }

    private static boolean checkcolum() {
        if (channel[0] == channel[3] && channel[6] != "7") {
            if (channel[0] == channel[6]) {
                return true;
            }}
        if (channel[1] == channel[4] && channel[7] != "8") {
            if (channel[1] == channel[7]) {
                return true;
            }}
        if (channel[2] == channel[5] && channel[8] != "9") {
            if (channel[2] == channel[8]) {
                return true;
            }}
        return false;
    }

    private static boolean checkDiagonal() {
        if (channel[0] == channel[4] && channel[8] != "9") {
            if (channel[0] == channel[8]) {
                return true;
            }
        }
        if (channel[2] == channel[4] && channel[6] != "7") {
            if (channel[2] == channel[6]) {
                return true;
            }
        }
        return false;
    }

    private static boolean isDraw() {
         if (turn == 8) {
            return true;
        }
        return false;
    }

    private static void switchPlayer() {
        if (player == "X") {
            player = "O";
        } else {
            player = "X";
        }
        turn++;
    }

    private static void showWin() {
        if (isDraw()) {
            System.out.println("Draw!!!  Thanks for playing. ");
        } else {
            System.out.println(player + " Win!!!!  Thanks for playing. ");
        }
    }

    private static void showBye() {
         System.out.println("See u later");
    }

    public static void main(String[] args) {
        showWelcome();
        setChannel();
        while (true) {
            showTable();
            showTurn();
            input();
            if (checkwin()) {
                break;
            }
            switchPlayer();
        }
        showTable();
        showWin();
        showBye();
    }
}
